<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
//    public function add_to_cart(Request $request) {
    public function add_to_cart($product_id, Request $request=NULL){
        
        if($request->qty == NULL){
            $qty=1;
        } else {
            $qty = $request->qty;
//            echo '-------------'.$qty;
        }
//        $product_id = $request->product_id;

        $product_info = DB::table('tbl_product')
                ->where('product_id', $product_id)
                ->first();
//        echo '<pre>';
//        print_r($product_info);
//        echo '</pre>';
        Cart::add(['id' => $product_info->product_id, 'name' => $product_info->product_name, 'qty' => $qty, 'price' => $product_info->price, 'options' => ['blog_image' => $product_info->blog_image]]);
//        return Redirect::to('/show-cart');
        return '<script>window.history.go(-1);</script>';
    }
    
    public function wishlist($id){
////        return 'xyz';
        $product_info = DB::table('tbl_product')
                ->where('product_id', $id)
                ->first();
        
        
//        return Redirect::to('/all-product');
        return '<script>window.history.go(-1);</script>';
    }

    public function show_cart() {
//        return 'xyz';

        $cart_page = view('pages.cart');
        return view('welcome')
                        ->with('content', $cart_page);
    }

    public function update_cart(Request $request) {
//        return 'xyz';
        $rowID = $request->row_Id;
        $qty = $request->qty;
//        echo $rowID ."<br>";
//        echo $qty;
        Cart::update($rowID, $qty); 
        return Redirect::to('/show-cart');
    }
    
    public function delete_cart($rowId){
//        return 'xyz';
        Cart::remove($rowId);
        return Redirect::to('/show-cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
