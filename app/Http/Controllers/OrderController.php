<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;

class OrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    public function manage_order() {
        $all_order = DB::table('tbl_order')
                ->join('tbl_customers', 'tbl_order.customer_id', '=', 'tbl_customers.customer_id')
                ->select('tbl_order.*', 'tbl_customers.*')
                ->orderBy('tbl_order.created_at', 'desc')
                ->get();

        $manage_order = view('admin.pages.order_list')
                ->with('all_order', $all_order);
        return view('admin.admin_master')
                        ->with('manage_order', $manage_order);
    }

    public function view_invoice($id) {
//        return $id;
        $order_info_by_id = DB::table('tbl_order')
                ->join('tbl_customers', 'tbl_order.customer_id', '=', 'tbl_customers.customer_id')
                ->join('tbl_payment', 'tbl_order.payment_id', '=', 'tbl_payment.payment_id')
                ->where('tbl_order.order_id', $id)
                ->select('tbl_order.*', 'tbl_customers.*', 'tbl_payment.*')
                ->first();
//        print_r($order_info_by_id);
//        exit();
        $shipping_info_by_id = DB::table('tbl_order')
                ->join('tbl_shipping', 'tbl_order.shipping_id', '=', 'tbl_shipping.shipping_id')
                ->where('tbl_order.order_id', $id)
                ->select('tbl_order.order_id', 'tbl_shipping.*')
                ->first();

        $order_details = DB::table('tbl_order_details')
                ->where('order_id', $id)
                ->get();

        $manage_invoice = view('admin.pages.invoice')
                ->with('order_info_by_id', $order_info_by_id)
                ->with('shipping_info_by_id', $shipping_info_by_id)
                ->with('order_details', $order_details);

        return view('admin.admin_master')
                        ->with('view_invoice', $manage_invoice);
    }

    public function delete_invoice($id) {
        //return 'xyz';
        DB::table('tbl_order')
                ->where('order_id', $id)
                ->delete();
        Session::flash('massage', 'Order deleted successfully');
        return Redirect::to('/manage-order');
    }

    public function view_customer_details($id) {
        $customer_details = DB::table('tbl_order')
                ->leftJoin('tbl_customers', 'tbl_order.customer_id', '=', 'tbl_customers.customer_id')
//                ->leftJoin('tbl_order','tbl_customers.customer_id' , '=', 'tbl_order.order_id')
                ->select( 'tbl_order.*','tbl_customers.*')
                ->where('tbl_order.order_id',$id)
                ->first();

//        echo '<pre>';
//        print_r($customer_details);
//        exit();


        $customer_info = view('admin.pages.customer_info')
                ->with('customer_details',$customer_details);
        return view('admin.admin_master')
                        ->with('customer_info', $customer_info);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
