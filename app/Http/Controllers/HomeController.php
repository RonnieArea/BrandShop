<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $feature_list = DB::table('tbl_product')
                ->where('product_status', 1)
                ->where('is_featured', 1)
                ->get();

        $slider = view('pages.slider');
        $category = view('pages.category');
//        $brands = view('pages.brands');
//        $popular_items = view('pages.popular_items');
        $feature_items = view('pages.feature_items')
                ->with('featured', $feature_list);


        return view('welcome')
                        ->with('slide', $slider)
                        ->with('category', $category)
                        ->with('feature_items', $feature_items)
//                ->with('brands',$brands)
//                ->with('popular_items',$popular_items)
        ;
    }

    public function all_product() {
//        return 'xyz';
        $all_product = DB::table('tbl_product')
                ->join('tbl_category', 'tbl_product.category_id', '=', 'tbl_category.category_id')
                ->where('tbl_product.product_status', 1)
                ->select('tbl_product.*', 'tbl_category.publication_status')
                ->get();

        $all_items = view('pages.all_products')
                ->with('all_product', $all_product);
        return view('welcome')
                        ->with('all_products', $all_items);
    }

    public function details($id) {
//        return 'xyz';
        $feature_detail = DB::table('tbl_product')
                ->where('product_id', $id)
                ->where('product_status', 1)
                ->first();

        $detailss = view('pages.details')
                ->with('fulldetails', $feature_detail);
        return view('welcome')
                        ->with('all_details', $detailss);
    }

    public function category_product($cat_id) {
//        return 'xyz';
        $cat_info = DB::table('tbl_product')
                ->where('category_id', $cat_id)
                ->get();
        $category_list = DB::table('tbl_category')
        ->where('publication_status', 1)
        ->get();

        $cat_product = view('pages.category_product')
                ->with('cat_info', $cat_info)
                ->with('category_list', $category_list);

        return view('welcome')
                        ->with('cat_product', $cat_product);
    }

    public function search() {
//        $search = $request->search;
        $searchterm = Input::get('searchinput');

        if ($searchterm) {

            $products = DB::table('tbl_product');
            $results = $products->where('product_name', 'LIKE', '%' . $searchterm . '%')
                    ->orWhere('short_description', 'LIKE', '%' . $searchterm . '%')
                    ->get();
            
            $search_page = view('pages.search')
                    ->with('v_products', $results);
            
            return view('welcome')->with('search)products', $search_page);
            
//        echo '<pre>';
//        var_dump($results);
//        exit();
        }
    }
    public function deliver_info(){
        $info_view = view('pages.delivery_info');
        return view('welcome')
                    ->with('delevery',$info_view);
    }

    public function return_policy(){
        $info_view = view('pages.return_policy');
        return view('welcome')
                    ->with('delevery',$info_view);
    }
    public function Contact_Us(){
        $info_view = view('pages.Contact_Us');
        return view('welcome')
                    ->with('delevery',$info_view);
    }
    
    public function contact_send(Request $request){
        $data = array();
        $data['user_name'] = $request->user_name;
        $data['user_email'] = $request->user_email;
        $data['user_message'] = $request->user_message;
        $data['created_at'] = date('Y-m-d H:i:s'); 
        DB::table('tbl_users_info')->insert($data);
        return Redirect::to('Contact-Us');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
