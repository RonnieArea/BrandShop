<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Session;
class CheckoutController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
    }

    public function checkout() {
//        return 'xyz';
        $checkoutview = view('pages.checkout');
        return view('welcome')->with('checkouts', $checkoutview);
    }

    public function ajax_email_check($email_address = null) {
//        echo '---------'.$email_address;
        $customer_info = DB::table('tbl_customers')
                ->where('email_address', $email_address)
                ->first();
//        echo '<pre>';
//        print_r($customer_info);
        if ($customer_info) {
            echo "Alreay exists";
        } else {
            echo "Available";
        }
    }

    public function save_customer(Request $request) {
//        return 'xyz';
        $data = array();
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['company_name'] = $request->company_name;
        $data['email_address'] = $request->email_address;

        $data['password'] = md5($request->password);
        $data['address'] = $request->address;
        $data['mobile'] = $request->mobile;
        $data['city'] = $request->city;
        $data['zip_code'] = $request->zip_code;
        $data['country'] = $request->country;
        $data['created_at'] = date('Y-m-d'); 

        $customer_id = DB::table('tbl_customers')->insertGetId($data);
//        echo $customer_id;
        Session::put('customer_id', $customer_id);
        return Redirect::to('/shipping-address');
    }


    public function shipping_address() {
//        return 'xyz';
        $shipping_page = view('pages.shipping');
        return view('welcome')->with('checkouts', $shipping_page);
    }

    public function save_shipping(Request $request) {
//        return 'xyz';
        $data = array();
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['company_name'] = $request->company_name;
        $data['email_address'] = $request->email_address;

        $data['address'] = $request->address;
        $data['mobile'] = $request->mobile;
        $data['city'] = $request->city;
        $data['zip_code'] = $request->zip_code;
        $data['country'] = $request->country;
//        $data['created_at'] = $request->country;
        $data['created_at'] = date('Y-m-d'); 
        $data['updated_at'] = date('Y-m-d'); 
        

        $shipping_id = DB::table('tbl_shipping')->insertGetId($data);
//        echo $customer_id;
        Session::put('shipping_id', $shipping_id);
        return Redirect::to('/payment');
    }

    public function payment() {
        $shippig_payment = view('pages.payment');

        return view('welcome')
                        ->with('content', $shippig_payment);
    }

    public function place_order(Request $request) {
//        return 'xyz';
        $payment_type = $request->payment_type;
        $data['payment_type'] = $payment_type;
        $payment_id = DB::table('tbl_payment')->insertGetId($data);
        /*
         * Start Order Save
         */
        $odata= array();
        $odata['customer_id'] = Session::get('customer_id') ;
        $odata['shipping_id'] = Session::get('shipping_id') ;
        $odata['payment_id'] = $payment_id;
       
             $order_total = str_replace(",","",Cart::total());
        $odata['order_total'] = $order_total;
        $odata['created_at'] = date('Y-m-d'); 
        $odata['updated_at'] = date('Y-m-d'); 
        
        $order_id = DB::table('tbl_order')->insertGetID($odata);
        
        /*
         * End Order Save
         */
        
        /*
         * Start Order Details Save
         */
        $oddata = array();
        $contents = Cart::content();
        foreach ($contents as $v_contents){
            $oddata['order_id'] = $order_id;
            $oddata['product_id'] = $v_contents->id;
            $oddata['product_name'] = $v_contents->name;
            $oddata['price'] = $v_contents->price;
            $oddata['product_sales_quantity'] = $v_contents->qty;
            $oddata['created_at'] = date('Y-m-d');
            $oddata['updated_at'] = date('Y-m-d');
            
             DB::table('tbl_order_details')->insert($oddata);
        }
        /* 
         * End Order Details Save
         */

        if ($payment_type == 'cash_on_delivery') {
            Cart::destroy();
            return Redirect::to('/order-successfull');
        }
//        if ($payment_type == 'paypal') {
//            return view('pages.htmlWebsiteStandardPayment');
//        }
    }

    
    public function order_successfull(){
        $successfull_layout = view('pages.successfull_order');
        return view('welcome')
                    ->with('successfull_order',$successfull_layout);
    }

    
    
    public function login_customer(Request $request){
        $email_address = $request->email_address;
        $password = md5($request->password);
        
        $result = DB::table('tbl_customers')
                ->where('email_address',$email_address)
                ->where('password',$password)
                ->first();
//        echo '<pre>';
//        print_r($result);
//        exit();
        if($result){
            return Redirect::to('shipping-address');
        } else {
            Session::put('exception','Username or password Invalid');
            return Redirect::to('checkout');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
