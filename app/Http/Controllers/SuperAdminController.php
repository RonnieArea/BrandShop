<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;

class SuperAdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $admin_id = session::get('id');
        if ($admin_id == null) {
            return redirect::to('/admin-panel')->send();
        }
        $home = view('admin.pages.home');
        return view('admin.admin_master')
                        ->with('home', $home);
//        return 'Ronnie';
    }

    public function add_category() {
        $add_category = view('admin.pages.add_category');
        return view('admin.admin_master')
                        ->with('content', $add_category);
    }

    public function save_category(Request $request) {
        $data = array();
        $data['category_name'] = $request->category_name;
        $data['category_description'] = $request->category_description;
        $data['publication_status'] = $request->publication_status;
        $data['created_at'] = date('Y-m-d'); 
        
        DB::table('tbl_category')->insert($data);
        Session::put('message', 'Save Category Information Successfully !');
        return redirect::to('/manage-category');
    }

    public function manage_category() {
        $cat_info = DB::table('tbl_category')->get();

        $manage_category = view('admin.pages.manage_category')
                ->with('category_info', $cat_info);
        return view('admin.admin_master')
                        ->with('content', $manage_category);
    }

    public function unpublished_category($id) {
        //return 'xyz';
        DB::table('tbl_category')
                ->where('category_id', $id)
                ->update(['publication_status' => 0]);
        Session::flash('message', 'Category Unpublished successfully');
        return Redirect::to('/manage-category');
    }

    public function published_category($id) {
        //return 'xyz';
        DB::table('tbl_category')
                ->where('category_id', $id)
                ->update(['publication_status' => 1]);
        Session::flash('message', 'Category published successfully');
        return Redirect::to('/manage-category');
    }

    public function edit_category($id) {
//        return 'xyz';
        $edit_category = DB::table('tbl_category')
                ->where('category_id', $id)
                ->first();
        $edit_cat_content = view('admin.pages.edit_category')
                ->with('edit_cat', $edit_category);
        return view('admin.admin_master')->with('content', $edit_cat_content);
    }

    public function update_category(Request $request) {
//        return 'xyz';
        $data = array();
        $category_id = $request->category_id;
        $data['category_name'] = $request->category_name;
        $data['category_description'] = $request->category_description;
        $data['publication_status'] = $request->publication_status;
        $data['updated_at'] = date('Y-m-d'); 

        DB::table('tbl_category')
                ->where('category_id', $category_id)
                ->update($data);
        Session::flash('message', 'Update Category Information Successfully !');
        return Redirect::to('/manage-category');
    }

    public function delete_category($id) {
        //return 'xyz';
        DB::table('tbl_category')
                ->where('category_id', $id)
                ->delete();
        Session::flash('message', 'Category published successfully');
        return Redirect::to('/manage-category');
    }

    public function add_product() {
        $categories = DB::table('tbl_category')
                ->where('publication_status', 1)
                ->get();
        $add_product = view('admin.pages.add_product')
                ->with('category_list', $categories);
        return view('admin.admin_master')
                        ->with('content', $add_product);
    }

    public function save_product(Request $request) {
        $data = array();
        $data['product_name'] = $request->product_name;
        $data['category_id'] = $request->category_id;
        $data['short_description'] = $request->short_description;
        $data['long_description'] = $request->long_description;
        $data['price'] = $request->price;
        $data['stock'] = $request->stock;
        
        if ($request->is_featured == 'on') {
            $data['is_featured'] = 1;
        }
        $data['product_status'] = $request->publication_status;
        $data['created_at'] = date('Y-m-d'); 
        
        //for image Upload
        $image = $request->file('blog_image');


        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'product_image/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
                $data['blog_image'] = $image_url;

                DB::table('tbl_product')->insertGetId($data);
                Session::put('massage', 'Save Product Information Successfully !');
                return redirect::to('/manage-product');
            }
        } else {
            DB::table('tbl_product')->insertGetId($data);
            Session::put('massage', 'Save Product Information Successfully !');
            return redirect::to('/manage-product');
        }
    }

    public function manage_product() {
//        return 'xyz';
        $pro_info = DB::table('tbl_product')->get();

        $manage_product = view('admin.pages.manage_product')
                ->with('product_info', $pro_info);
        return view('admin.admin_master')
                        ->with('content', $manage_product);
    }

    public function unpublished_product($id) {
//        return 'xyz';
        DB::table('tbl_product')
                ->where('product_id', $id)
                ->update(['product_status' => 0]);
        Session::flash('massage', 'Product Unpublished successfully');
        return Redirect::to('/manage-product');
    }

    public function published_product($id) {
//        return 'xyz';
        DB::table('tbl_product')
                ->where('product_id', $id)
                ->update(['product_status' => 1]);
        Session::flash('massage', 'Product published successfully');
        return Redirect::to('/manage-product');
    }

    public function edit_product($id) {
//        return 'xyz';

        $blog_info = DB::table('tbl_product')
                ->leftJoin('tbl_category', 'tbl_product.category_id', '=', 'tbl_category.category_id')
                ->where('product_id', $id)
//                ->select('tbl_blog.*', 'tbl_category.category_name')
                ->first();

        $category_info = DB::table('tbl_category')
                ->where('publication_status', 1)
                ->get();

        $edit_pro_content = view('admin.pages.edit_product')
                ->with('pro_info', $blog_info)
                ->with('cat_list', $category_info);

        return view('admin.admin_master')->with('content', $edit_pro_content);
    }

    public function update_product(Request $request) {
//        return 'xyz';
        $data = array();
        $product_id = $request->product_id;
        $data['product_name'] = $request->product_name;
        $data['category_id'] = $request->category_id;
        $data['short_description'] = $request->short_description;
        $data['long_description'] = $request->long_description;
        $data['price'] = $request->price;
        $data['stock'] = $request->stock;

        if ($request->is_featured == 'on') {
            $data['is_featured'] = 1;
        }

        $data['product_status'] = $request->publication_status;
        $data['updated_at'] = date('Y-m-d'); 
        

        //for image Upload
        $image = $request->file('blog_image');

        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'product_image/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
                $data['blog_image'] = $image_url;
                DB::table('tbl_product')
                        ->where('product_id', $product_id)
                        ->update($data);
                Session::put('message', 'Update Product with Image Successfully');
                return redirect::to('/manage-product');
            }
        } else {
            DB::table('tbl_product')
                    ->where('product_id', $product_id)
                    ->update($data);
            Session::put('message', 'Update Product without image Successfully');
            return redirect::to('/manage-product');
        }
    }

    public function delete_product($id) {
//        return 'xyz';
        DB::table('tbl_product')
                ->where('product_id', $id)
                ->delete();
        Session::flash('massage', 'Product Delete successfully');
        return Redirect::to('/manage-product');
    }
    public function contact_sms(){
//        return 'xyz';
        $envelove_info = DB::table('tbl_users_info')->get();
        
        $envelove = view('admin.pages.envelove')
                ->with('envelove_info',$envelove_info);
        
        return view('admin.admin_master')
                    ->with('envelove',$envelove);
    }
    public function view_sms($id){
//        return 'xyz';
        $envelove_ss = DB::table('tbl_users_info')
                ->where('user_id', $id)
                ->first();
        $en = view('admin.pages.envelove_details')
                ->with('info',$envelove_ss);
        
        return view('admin.admin_master')
                    ->with('envelove',$en);
//        return Redirect::to('/contact-sms');
    }
    public function delete_sms($id){
//        return 'xyz';
        DB::table('tbl_users_info')
                ->where('user_id', $id)
                ->delete();
        Session::flash('message', 'Message Delete successfully');
        return Redirect::to('/contact-sms');
    }

    public function logout() {
        Session::put('name', null);
        Session::put('id', null);
        Session::put('message', 'You are successfully Logout');
        return redirect::to('/admin-panel');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
