<?php

/*
 * ecommerce admin controller
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use Session;

class AdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $admin_id = Session::get('id');
        if($admin_id != null){
            return redirect::to('/dashboard')->send();
        }
        return view('admin.login');
    }

    public function admin_login_check(Request $request) {
//        return view('admin.admin_master');
//        return 'admin panel';
         $admin_email_adress = $request->admin_email_address;
        $admin_password = md5($request->admin_password);
//        $admin_password = $request->admin_password;
//        $admin_password = bcrypt($request->admin_password);

        $result = DB::table('tbl_admin')
                ->where('email', $admin_email_adress)
                ->where('password', $admin_password)
                ->first();
        if($result){
            Session::put('name', $result->name);
            Session::put('id', $result->id);
            return redirect::to('/dashboard');
        } else {
            session::put('exception', 'Username or Password Invalid');
            return redirect::to('/admin-panel');
        }
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
