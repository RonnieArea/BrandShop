<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\SuperAdminController;
use App\Http\Controllers\AdminController;

use Illuminate\Support\Facades\Redirect;

class authenticateMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
//    public function handle($request, Closure $next)
//    {
//        return $next($request);
//    }
    public function handle($request, Closure $next) {
//        if (SuperAdminController::check()) {
            return $next($request);
//        } else {
//            return Redirect::to('admin-panel');
//        }
    }

}
