<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
 
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'category_name' => $faker->name,
        'category_description' => $faker->text,
        'password' => $faker->text,
        'publication_status' => $faker->boolean,
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {

    return [
        'product_name' => $faker->title,
        'category_id' => $faker->randomDigitNotNull,
        'blog_image' => $faker->image,
        'short_description' => $faker->sentence,
        'long_description' => $faker->text,
        'price' => $faker->randomDigit,
        'stock' => $faker->randomDigit,
        'is_featured' => $faker->boolean,
        'product_status' => $faker->boolean,
    ];
});

