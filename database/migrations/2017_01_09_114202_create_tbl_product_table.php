<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProductTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tbl_product', function (Blueprint $table) {
            $table->increments('product_id');
            $table->string('product_name');
            $table->integer('category_id');
            $table->string('blog_image')->nullable();
            //blog_image will set in db default (as defined null)
            $table->text('short_description');
            $table->text('long_description');
            $table->double('price')->nullable();
            $table->integer('stock')->default(0);
            
            $table->tinyInteger('is_featured')->default(0);
            //is_featured will set in db default (as defined 0)
            $table->tinyInteger('product_status')->default(0);;
            //product_status will set in db default (as defined 0)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tbl_product');
    }

}
