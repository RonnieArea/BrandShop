<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Login</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="{{URL::to('public/admin_asset/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{URL::to('public/admin_asset/assets/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet" />
        <link href="{{URL::to('public/admin_asset/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
        <link href="{{URL::to('public/admin_asset/css/style.css')}}" rel="stylesheet" />
        <link href="{{URL::to('public/admin_asset/css/style-responsive.css')}}" rel="stylesheet" />
        <link href="{{URL::to('public/admin_asset/css/style-default.css')}}" rel="stylesheet" id="style_color" />
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>
		
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="lock">
        <div class="lock-header">
            <!-- BEGIN LOGO -->
            <a class="center" id="logo" href="{{URL::to('/')}}"  style="text-decoration: none;font-size: 22px; text-transform:uppercase">
                <h1 style="color: #fff; "> Organicbeauty-Care</h1>
            </a>
            <!-- END LOGO -->
        </div>
               
        <h2 style="color: #e55155;text-align: center;" class="flashmessage">
    <?php
    $exception = Session::get('exception');
    if($exception){
        echo $exception;
        Session::put('exception', null);
    }
    ?>
</h2>
        <h2 style="color: #a0ea70; text-align: center;" class="flashmessage">
            <?php
                $message = Session::get('message');
                if($message){
                    echo $message;
                    Session::put('message', null);
                }
            ?>
        </h2>
        <div class="login-wrap">
            <div class="metro single-size red">
                <div class="locked">
                    <i class="icon-lock"></i>
                    <span>Login</span>
                </div>
            </div>

<!-----login form start------>
     
            {!! Form::open(['url' => '/admin-login-check', 'method' => 'post', 'class' => 'form-horizonal']) !!}
                
                <div class="metro double-size green">
                    <div class="input-append lock-input">
                        <input type="text" class="" placeholder="Username" name="admin_email_address">
                    </div>
                </div>
                
                <div class="metro double-size yellow">
                    <div class="input-append lock-input">
                        <input type="password" class="" placeholder="Password" name="admin_password">
                    </div>
                </div>
                
                <div class="metro single-size terques login">
                    
                        <button type="submit" class="btn login-btn">
                            Login
                            <i class=" icon-long-arrow-right"></i>
                        </button>
                </div>
            {!! Form::close() !!}       
                      
            <!-----login form End------>
            
			<!--
			
                <div class="metro double-size navy-blue ">
                    <a href="index.html" class="social-link">
                        <i class="icon-facebook-sign"></i>
                        <span>Facebook Login</span>
                    </a>
                </div>
                <div class="metro single-size deep-red">
                    <a href="index.html" class="social-link">
                        <i class="icon-google-plus-sign"></i>
                        <span>Google Login</span>
                    </a>
                </div>
                <div class="metro double-size blue">
                    <a href="index.html" class="social-link">
                        <i class="icon-twitter-sign"></i>
                        <span>Twitter Login</span>
                    </a>
                </div>
                <div class="metro single-size purple">
                    <a href="index.html" class="social-link">
                        <i class="icon-skype"></i>
                        <span>Skype Login</span>
                    </a>
                </div>
				
				---->
                <div class="login-footer">
                    <div class="remember-hint pull-left">
                        <input type="checkbox" id=""> Remember Me
                    </div>
                    <div class="forgot-hint pull-right">
                        <a id="forget-password" class="" href="javascript:;">Forgot Password?</a>
                    </div>
                </div>
        </div>
		
        
<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script>
    </body>
    <!-- END BODY -->
</html>