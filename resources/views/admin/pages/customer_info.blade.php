@extends('admin.admin_master')
@section('content')
<style>
th{max-width: 20px;}
td{max-width: 80px;}
</style>
<div class="container">
        <div class="customer_data text-center col-md-6" >
            <h2 class="heading">Customer Information</h2>
            <table class="table table-striped table-bordered" id="sample_1">
                <tr class="">
                    <th>First name</th>
                    <td>{{$customer_details->first_name}}</td>
                </tr>
                <tr>
                    <th>Last name</th>
                    <td>{{$customer_details->last_name}}</td>
                </tr>
                <tr>
                    <th>Company Name</th>
                    <td>{{$customer_details->company_name}}</td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>{{$customer_details->email_address}}</td>
                </tr>
                <tr>
                    <th>Address:</th>
                    <td>{{$customer_details->address}}</td>
                </tr>
                <tr>
                    <th>Mobile:</th>
                    <td>{{$customer_details->mobile}}</td>
                </tr>
                <tr>
                    <th>City:</th>
                    <td>{{$customer_details->city}}</td>
                </tr>
                <tr>
                    <th>Zip Code:</th>
                    <td>{{$customer_details->zip_code}}</td>
                </tr>
            </table>
        </div>
    </div>

@stop
