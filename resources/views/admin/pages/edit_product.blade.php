@extends('admin.admin_master')
@section('content')
<!-- BEGIN PAGE HEADER-->   

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
            <span class="settings">
                <span class="text">Theme Color:</span>
                <span class="colors">
                    <span class="color-default" data-style="default"></span>
                    <span class="color-green" data-style="green"></span>
                    <span class="color-gray" data-style="gray"></span>
                    <span class="color-purple" data-style="purple"></span>
                    <span class="color-red" data-style="red"></span>
                </span>
            </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            ADD PRODUCT
        </h3>
        <ul class="breadcrumb">

            <a href="{{URL::to('/manage-product')}}" class="btn">All Products</a> 

        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN SAMPLE FORMPORTLET-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> ADD PRODUCT Form </h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <!-- BEGIN FORM-->
                {!! Form::open(['url' => '/update-product', 'class'=>'form-horizontal', 'role' => 'form', 'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
                <form action="#" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">Product Name</label>
                        <div class="controls">
                            <input type="text" name="product_name" class="span6 " value="{{$pro_info->product_name}}"/>
                        </div>
                    </div>
                    <input type="hidden" name="product_id" value="{{$pro_info->product_id}}"/>


                    <div class="control-group">
                        <label class="control-label">Category</label>
                        <div class="controls">
                            <select name="category_id" class="chzn-select-deselect span6" >
                                <option value="{{$pro_info->category_id}}">Select Category</option>
                                @foreach($cat_list as $single_cat)
                                <option value="{{$single_cat->category_id}}"> {{$single_cat->category_name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Product short description</label>
                        <div class="controls">
                            <textarea class="span6 " name="short_description" rows="3">{{$pro_info->short_description}}</textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Product long description</label>
                        <div class="controls">
                            <textarea class="span6 " name="long_description" rows="3">{{$pro_info->long_description}}</textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"> Price</label>
                        <div class="controls">
                            <input type="number" name="price" class="span6 " value="{{$pro_info->price}}" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Stock</label>
                        <div class="controls">
                            <input type="number" name="stock" class="span6 " value="{{$pro_info->stock}}" />

                        </div>
                    </div>

                    <div class="control-group code">
                        <label class="control-label" for="fileInput">Image Upload</label>
                        <div class="controls">
                            <input class="input-file uniform_on" id="fileInput" type="file" name="blog_image">
                        
                                @if($pro_info->blog_image == null)
                                    <p style="color: #f34f4f;">no Image here</p>
                                    @else
                                    <img src="{{ asset($pro_info->blog_image) }}" width="80px" height="20px" alt=""/>
                                    @endif
                                
                        </div>
                               
                    </div>  

                
                    <div class="control-group">
                        <label class="control-label" for="appendedPrependedInput">Is Featured</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <input type="checkbox" name="is_featured">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Publication Status</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="publication_status" id="optionsRadios1" value="1" checked="">
                                Publish
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="publication_status" id="optionsRadios2" value="0">
                                Unpublish
                            </label>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn">Cancel</button>
                    </div>
                    {!! Form::close() !!} 
                    <!-- END FORM-->
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

@endsection
