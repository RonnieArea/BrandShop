@extends('admin.admin_master')
@section('content')

<!-- BEGIN PAGE HEADER-->   
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
            <span class="settings">
                <span class="text">Theme Color:</span>
                <span class="colors">
                    <span class="color-default" data-style="default"></span>
                    <span class="color-green" data-style="green"></span>
                    <span class="color-gray" data-style="gray"></span>
                    <span class="color-purple" data-style="purple"></span>
                    <span class="color-red" data-style="red"></span>
                </span>
            </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Invoice
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
                <span class="divider">/</span>
            </li>
            <li>
                <a href="#">Extra</a>
                <span class="divider">/</span>
            </li>
            <li class="active">
                Invoice
            </li>
            <li class="pull-right search-wrap">
                <form action="search_result.html" class="hidden-phone">
                    <div class="input-append search-input-area">
                        <input class="" id="appendedInputButton" type="text">
                        <button class="btn" type="button"><i class="icon-search"></i> </button>
                    </div>
                </form>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN BLANK PAGE PORTLET-->
        <div class="widget purple">
            <div class="widget-title">
                <h4><i class="icon-edit"></i> Invoice Page </h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="text-center">
<!--                            <img alt="" src="{{URL::to('public/images/home/logo.png')}}">-->
                            <a href="{{URL::to('/')}}" style="color: #EB5858"><h1><span>B</span>rand-showroom</h1></a>
                        </div>
                        <hr>

                    </div>
                </div>
                <div class="space20"></div>
                <div class="row-fluid invoice-list">
                    <div class="span4">
                        <h4>BILLING ADDRESS</h4>
                        <p>
                            <br>
                            H 57/2, Sector 5 <br>
                            Uttara, Dhaka<br>
                            88 01961816803
                        </p>
                    </div>
                    <div class="span4">
                        <h4>SHIPPING INFORMATION</h4>
                        <p>
                            Name: {{$shipping_info_by_id->first_name.' '.$shipping_info_by_id->last_name}}<br>
                            Adress:{{$shipping_info_by_id->address}}<br>
                            {{$shipping_info_by_id->city}} {{$shipping_info_by_id->zip_code}}<br>
                            MOB: {{$shipping_info_by_id->mobile}}<br>
                        </p>
                    </div>
                    <div class="span4">
                        <h4>INVOICE INFO</h4>
                        <ul class="unstyled">
                            <li>Invoice Number		: <strong>#00{{$order_info_by_id->order_id}}</strong></li>
                            <li>Invoice Date		: {{$order_info_by_id->created_at}}</li>
                            <?php
                            if ($order_info_by_id->payment_status == 0){
                                
                                ?>
                            <li>Invoice Status      : Pending</li>
                            <?php } else{ ?>
                            <li>Invoice Status      : Delivered</li>
                            <?php }?>
                            
                        </ul>
                    </div>
                </div>
                <div class="space20"></div>
                <div class="space20"></div>
                <div class="row-fluid">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th class="hidden-480">Product Price</th>
                                <th class="hidden-480">Quantity</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($order_details as $single_deatils)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$single_deatils->product_name}}</td>
                                <td class="hidden-480">{{$single_deatils->price}} Tk</td>
                                <td class="hidden-480">{{$single_deatils->product_sales_quantity}}</td>
                                <td>{{$single_deatils->price * $single_deatils->product_sales_quantity}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="space20"></div>
                <div class="row-fluid">
                    <div class="span4 invoice-block pull-right">
                        <ul class="unstyled amounts">
                            <li><strong>Sub - Total amount :</strong> {{$order_info_by_id->order_total}} Tk.</li>
                            <li><strong>Shipping Cost  :</strong> free</li>
                            <li><strong>Grand Total :</strong> {{$order_info_by_id->order_total}} Tk.</li>
                        </ul>
                    </div>
                </div>
                <div class="space20"></div>
                <div class="row-fluid text-center">
                    <!--<a class="btn btn-success btn-large hidden-print"> Submit Your Invoice <i class="icon-check"></i></a>-->
                    <a class="btn btn-inverse btn-large hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
                </div>
            </div>
        </div>
        <!-- END BLANK PAGE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->

@endsection