@extends('admin.admin_master')
@section('content')

<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN BASIC PORTLET-->
        <div class="widget orange">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Message Table</h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <?php
            $message = Session::get('message');
            if (isset($message)) {
                ?>
                <div class="alert alert-block alert-success fadein">
                    <button data-dismiss="alert" class="close" type="button"></button>
                    <h4 class="alert-heading">Success!</h4>
                    <p><?php echo $message; ?></p>
                </div>
            <?php } ?>
            <div class="widget-body">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th> No.</th>
                            <th class="hidden-phone"><i class="icon-question-sign"></i> Name</th>
                            <th><i class="icon-bookmark"></i> Email</th>
                            <th>Sent Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach($envelove_info as $v_envelove)
                        <tr>
                            <td>{{++$i}}</td>
                            <td><p>{{$v_envelove->user_name}}</p></td>
                            <td class="hidden-phone">{{$v_envelove->user_email}}</td>
                            <td class="hidden-phone">{{$v_envelove->created_at}}</td>

                            <td>
                                <a href="{{URL::to('/view-sms/'.$v_envelove->user_id)}}"><button class="btn btn-primary"><i class="icon-eye-open"></i></button></a>
                                <!--<a href="{{$v_envelove->user_id}}"><button class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><i class="icon-eye-open"></i></button></a>-->

                                <a href="{{URL::to('/delete-sms/'.$v_envelove->user_id)}} " onclick="return checkDelete()"><button class="btn btn-danger"><i class="icon-trash"></i></button></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BASIC PORTLET-->
    </div>
</div>


@stop
