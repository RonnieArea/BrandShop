@extends('admin.admin_master')
@section('content')
 <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Dashboard
                            </h3>
                            <ul class="breadcrumb">
                                <li>
                                    <a href="#">Home</a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <!--BEGIN METRO STATES-->
                        <div class="metro-nav">
                            <div class="metro-nav-block nav-block-orange">
                                <a data-original-title="" href="#">
                                    <i class="icon-user"></i>
                                    <?php 
                                    $user = DB::table('tbl_customers')->get();
                                    ?>
                                    <div class="info"><?php echo count($user) ;?></div>
                                    <div class="status">Total User</div>
                                </a>
                            </div>
<!--                            <div class="metro-nav-block nav-block-yellow">
                                <a data-original-title="" href="#">
                                    <i class="icon-tags"></i>
                                    <div class="info">+970</div>
                                    <div class="status">Sales</div>
                                </a>
                            </div>-->
                        </div>
                        <div class="metro-nav">
                            <div class="metro-nav-block nav-block-blue">
                                <a data-original-title="" href="#">
                                    <i class="icon-shopping-cart"></i>
                                    <?php 
                                    
                                    $date = date('Y-m-d');
//                                    echo date('d');
                                    $order = DB::table('tbl_order')
                                            ->whereDate('created_at',$date)
//                                            ->where('created_at',$date)
                                            ->get();
                                    ?>
                                    <div class="info"><?php echo count($order) ;?></div>
                                    <div class="status">New Order</div>
                                </a>
                            </div>
<!--                            <div class="metro-nav-block nav-block-green double">
                                <a data-original-title="" href="#">
                                    <i class="icon-tasks"></i>
                                    
                                    <div class="info">0</div>
                                   
                                    <div class="status">Stock</div><br>
                                </a>
                            </div>-->
<!--                            <div class="metro-nav-block nav-block-grey ">
                                <a data-original-title="" href="#">
                                    <i class="icon-external-link"></i>
                                    <div class="info">$53412</div>
                                    <div class="status">Total Profit</div>
                                </a>
                            </div>-->
                        </div>
                        <div class="space10"></div>
                        <!--END METRO STATES-->
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- BEGIN CHART PORTLET-->
                            
                            <!-- END CHART PORTLET-->
                        </div>
                        <div class="span6">
                            <!-- BEGIN CHART PORTLET-->
                            <!-- END CHART PORTLET-->
                        </div>
                    </div>


                    <!-- END PAGE CONTENT-->  
@stop
