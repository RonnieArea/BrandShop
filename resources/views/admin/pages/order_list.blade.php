@extends('admin.admin_master')
@section('content')
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->   
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN THEME CUSTOMIZER-->
            <div id="theme-change" class="hidden-phone">
                <i class="icon-cogs"></i>
                <span class="settings">
                    <span class="text">Theme Color:</span>
                    <span class="colors">
                        <span class="color-default" data-style="default"></span>
                        <span class="color-green" data-style="green"></span>
                        <span class="color-gray" data-style="gray"></span>
                        <span class="color-purple" data-style="purple"></span>
                        <span class="color-red" data-style="red"></span>
                    </span>
                </span>
            </div>
            <!-- END THEME CUSTOMIZER-->
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Order Table
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="{{URL::to('/')}}">Home</a>
                    <span class="divider">/</span>
                </li>
                <li class="active">
                    Editable Table
                </li>
                <li class="pull-right search-wrap">
                    <form action="http://thevectorlab.net/metrolab/search_result.html" class="hidden-phone">
                        <div class="input-append search-input-area">
                            <input class="" id="appendedInputButton" type="text">
                            <button class="btn" type="button"><i class="icon-search"></i> </button>
                        </div>
                    </form>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN ADVANCED TABLE widget-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE widget-->
            <div class="widget red">
                <div class="widget-title">
                    <h4><i class="icon-reorder"></i> Order Table</h4>
                    <span class="tools">
                        <a href="javascript:;" class="icon-chevron-down"></a>
                        <a href="javascript:;" class="icon-remove"></a>
                    </span>
                </div>
                <div class="widget-body">
                    <?php
                    $massage = Session::get('massage');
                    if (isset($massage)) {
                        ?>
                        <div class="alert alert-block alert-success fadein">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <h4 class="alert-heading">Success!</h4>
                            <p><?php echo $massage; ?></p>
                        </div>
                    <?php } ?>
                    <table class="table table-striped table-bordered" id="sample_1">
                        <thead>
                            <tr>
                                <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                                <th>Order ID</th>
                                <th class="hidden-phone">Customer</th>
                                <th class="hidden-phone">Status</th>
                                <th class="hidden-phone">Total (BDT)</th>
                                <th class="hidden-phone">Date Added</th>
                                <th class="hidden-phone">Date Modified</th>
                                <th class="hidden-phone">action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($all_order as $v_order)
                            <tr class="odd gradeX">
                                <td><input type="checkbox" class="checkboxes" value="1" /></td>
                                <td><?php echo ++$i; ?></td>
                                <td>{{$v_order->first_name}}</td>
                                <?php
                                if ($v_order->order_status == 0) {
                                    ?>
                                    <td class="hidden-phone"><span class="label label-success">Pending</span></td>
                                <?php } else { ?>
                                    <td class="hidden-phone"><span class="label label-danger">Deliverd</span></td>
                                <?php } ?>


                                <td class="hidden-phone">{{$v_order->order_total}}</td>
                                <td class="center hidden-phone">{{$v_order->created_at}}</td>
                                <td class="center hidden-phone">{{$v_order->updated_at}}</td>

                                <td>

                                    <a class="btn btn-success" href="{{URL::to('/view-invoice/'.$v_order->order_id)}}">
                                        <i class="halflings-icon white  icon-eye-open"></i>  
                                    </a>

                                    <a class="btn btn-info" href="{{URL::to('/view-customer-details/'.$v_order->order_id)}}">
                                        <i class="halflings-icon icon-pencil"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="{{URL::to('/delete-invoice/'.$v_order->order_id)}}" onclick="return checkDelete()">
                                        <i class="halflings-icon icon-trash"></i> 
                                    </a>


                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE widget-->
        </div>
    </div>

    <!-- END ADVANCED TABLE widget-->
</div>
<!-- END PAGE CONTAINER-->
@endsection