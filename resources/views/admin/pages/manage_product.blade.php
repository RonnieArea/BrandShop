@extends('admin.admin_master')
@section('content')
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<h3 class="page-title">
    Editable Table
</h3>
<ul class="breadcrumb">
    <li>
        <a href="#">Home</a>
        <span class="divider">/</span>
    </li>
    <li>
        <a href="#">Data Table</a>
        <span class="divider">/</span>
    </li>
    <li class="active">
        Editable Table
    </li>
    <li class="pull-right search-wrap">
        <form action="http://thevectorlab.net/metrolab/search_result.html" class="hidden-phone">
            <div class="input-append search-input-area">
                <input class="" id="appendedInputButton" type="text">
                <button class="btn" type="button"><i class="icon-search"></i> </button>
            </div>
        </form>
    </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN EDITABLE TABLE widget-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE widget-->
        <div class="widget purple">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Editable Table</h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <?php
            $message = Session::get('message');
            if (isset($message)) {
                ?>
                <div class="alert alert-block alert-success fadein">
                    <button data-dismiss="alert" class="close" type="button"></button>
                    <h4 class="alert-heading">Success!</h4>
                    <p><?php echo $message; ?></p>
                </div>
            <?php } ?>
            
            <?php
                    
            ?>
            <div class="widget-body">
                <div>
                    <div class="clearfix">

                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Product Name</th>
                                <th>Product Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0 ?>
                            @foreach($product_info as $single_cat)
                            <tr class="center">
                                <td>{{ ++$i}}</td>
                                <td>{{$single_cat->product_name}}</td>
                                <td>
                                    @if($single_cat->blog_image == null)
                                    <p style="color: #f34f4f;">no Image here</p>
                                    @else
                                    <img src="{{ asset($single_cat->blog_image) }}" width="100px" height="50px" alt=""/>
                                    @endif
                                </td>

                                <td> 
                                    @if($single_cat->product_status == 1)
                                    <span class="label label-success">Published</span>
                                    @else
                                    <span class="label label-inverse">Unpublished</span>
                                    @endif
                                </td>
                                
                                <td>
                                    @if($single_cat->product_status==1)
                                    <a class="btn btn-success" href="{{URL::to('unpublished-product/'.$single_cat->product_id)}}">
                                        <i class="halflings-icon white  icon-thumbs-up"></i>  
                                    </a>
                                    @else
                                    <a class="btn btn-danger" href="{{URL::to('published-product/'.$single_cat->product_id)}}">
                                        <i class="halflings-icon white icon-thumbs-down"></i>  
                                    </a>
                                    @endif
                                    <a class="btn btn-info" href="{{URL::to('edit-product/'.$single_cat->product_id)}}">
                                        <i class="halflings-icon icon-pencil"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="{{URL::to('delete-product/'.$single_cat->product_id)}}" onclick="return checkDelete()">
                                        <i class="halflings-icon icon-trash"></i> 
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE widget-->
    </div>
</div>

<!-- END EDITABLE TABLE widget-->

@stop


