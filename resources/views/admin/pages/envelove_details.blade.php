@extends('admin.admin_master')
@section('content')
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN BASIC PORTLET-->
        <div class="widget orange">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> View Message </h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
        </div>
    </div>
</div>


        <div class="row-fluid">
            <ul class="metro_tmtimeline">
                <li class="green" style="list-style: none">
                    <div class="metro_tmtime" datetime="2013-04-10 18:30">
                        <span class="date">{{$info->created_at}}</span>
                    </div>
                    <div class="metro_tmicon">
                    </div>
                    <div class="metro_tmlabel">
                        <h2> {{$info->user_name}}</h2>
                        <p>{{$info->user_message}}</p>
                        <a href="#">{{$info->user_email}}</a><br><br>
                        <a href="{{URL::to('/contact-sms')}}"><button class="btn btn-danger add-to-cart">GO Back <i class="icon-circle-arrow-left"></i></button></a>
                    </div>
                </li>
            </ul>
        </div>

@stop

