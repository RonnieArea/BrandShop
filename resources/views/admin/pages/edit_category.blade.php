@extends('admin.admin_master')
@section('content')
<!-- BEGIN PAGE HEADER-->   
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
            <span class="settings">
                <span class="text">Theme Color:</span>
                <span class="colors">
                    <span class="color-default" data-style="default"></span>
                    <span class="color-green" data-style="green"></span>
                    <span class="color-gray" data-style="gray"></span>
                    <span class="color-purple" data-style="purple"></span>
                    <span class="color-red" data-style="red"></span>
                </span>
            </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Edit Category
        </h3>
        <ul class="breadcrumb">

            <a href="{{URL::to('/manage-category')}}" class="btn">Manage Category</a> 

        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN SAMPLE FORMPORTLET-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Edit Category </h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <div class="widget-body">

                <!-- BEGIN FORM-->
                {!! Form::open(array('url'=>'update-category', 'class'=>'form-horizontal', 'role' => 'form', 'method' => 'POST','enctype'=>'multipart/form-data')) !!}


                <div class="control-group">
                    <label class="control-label">Category Name</label>
                    <div class="controls">
                        <input type="text" name="category_name" class="span6 " value="{{$edit_cat->category_name}}"/>
                    </div>
                </div>
                <input type="hidden" name="category_id" value="{{$edit_cat->category_id}}"/>

                <div class="control-group">
                    <label class="control-label">Category Description</label>
                    <div class="controls">
                        <textarea class="span6 " name="category_description" rows="3">
                            {{$edit_cat->category_description}}
                        </textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"> Select Category</label>
                    <div class="controls">
                        <select data-placeholder="Your Favorite Type of Bear" name="publication_status" class="chzn-select-deselect span6" tabindex="-1" id="selCSI">
                            <option value="{{$edit_cat->publication_status}}">SELECT</option>
                            <option value="1">Published</option>
                            <option value="2">Unpublished</option>
                        </select>
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn">Cancel</button>
                </div>
                {!! Form::close() !!} 
                <!-- END FORM-->
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

@endsection

