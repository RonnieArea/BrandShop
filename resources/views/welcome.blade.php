<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>@yield('title_name') | Ronnie Area</title>

        <link href="{{asset('public/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('public/css/prettyPhoto.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/price-range.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/jquery.bxslider.css')}}" rel="stylesheet">

        <link href="{{asset('public/css/main.css')}}" rel="stylesheet"  type="text/css">
		
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="{{asset('public/css/share/plugin.css')}}"  rel="stylesheet" type="text/css" media="all">       

        <link href="{{asset('public/css/responsive.css')}}" rel="stylesheet">
    </head><!--/head--->

    <body>
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#">Enquiry : <span></span><i class="fa fa-phone"></i> +88 018XX XXX XXX</a></li>
                                    <li><a href="#">Sales : <span></span><i class="fa fa-phone"></i> +88 017XX XXX XXX</a></li>
                                    <li><a href="{{URL::to('/deliver-info')}}"><i class="fa fa-truck" aria-hidden="true"></i> Delivery Info</a></li>
                                    <li><a href="{{URL::to('/return-policy')}}"><i class="fa fa-mail-reply"></i> Returns Policy</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
        <!--                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left" >
                                <a href="{{URL::to('/')}}" style="color: #EB5858">
                                    <h1><span><img src="{{ asset('public/images/xxxxxxxxxxxxxdwgg.png') }}"  alt="" /> </h1>
                                </a>
                            </div>
                            <div class="btn-group pull-right">

                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="{{URL::to('/checkout')}}"><i class="fa fa-crosshairs"></i> Checkout  </a></li>
                                    <li><a href="{{URL::to('/show-cart')}}"><i class="fa fa-shopping-cart"></i> Cart <?php echo Cart::count();?></a></li>

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-user"></i>My Account <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <?php
                                            $customer_id = Session::get('customer_id');
                                            if ($customer_id != NULL) {
//                                            
                                                ?>
                                                <li><a href="#"><i class="fa fa-unlock" aria-hidden="true"></i> Log Out</a></li>
                                            <?php } else { ?>
                                                <li><a href="#"><i class="fa fa-lock" aria-hidden="true"></i> Login </a></li>                
                                            <?php } ?>
                                            <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="{{URL::to('/')}}" class="active">Home</a></li>
                                    <li><a href="{{URL::to('/all-product')}}">All Product</a></li>
                                    <?php
                                    $category_list = DB::table('tbl_category')
                                            ->where('publication_status', 1)
                                            ->get();
                                    ?>
                                    @foreach($category_list as $cat_list)
                                    <li class=""><a href="{{URL::to('/category-product/'.$cat_list->category_id)}}"> {{$cat_list->category_name}}</a></li>
                                    @endforeach
                                    <li><a href = "{{URL::to('/Contact-Us')}}">Contact Us</a></li>
                                    <!--<li class = "dropdown"><a href = "#">Blog<i class = "fa fa-angle-down"></i></a>
                                    <ul role = "menu" class = "sub-menu">
                                    <li><a href = "blog.html">Blog List</a></li>
                                    <li><a href = "blog-single.html">Blog Single</a></li>
                                    </ul>
                                    </li> -->
                                    <!--<li><a href = "contact-us.html">Contact</a></li> -->
                                </ul>

                            </div>
                        </div>
                        <div class = "col-sm-3">
                            {!!Form::open(['url' => '/search', 'method' => 'get'])!!}
                            <div class = "search_box pull-right">
                                <input type = "text" name = "searchinput" placeholder = "Search"/>
                            </div>
                            {!!Form::close()!!}
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->

        @yield('content')

        <section id = "slider"><!--slider-->
            <div class = "container">
                <div class = "row">

                    @yield('slider')

                </div>
            </div>
        </section><!--/slider-->

        <section>
            <div class = "container">
                <div class = "row">
                    <div class = "col-sm-3">
                        <div class = "left-sidebar">
                            @yield('category')

                            <div class = "brands_products"><!--brands_products-->
                                @yield('brands')

                            </div><!--/brands_products-->


                            <div class = "shipping text-center"><!--shipping-->
                                @yield('add')
                            </div><!--/shipping-->

                        </div>
                    </div>

                    <div class = "col-sm-9 padding-right">
                        <div class = "features_items"><!--features_items-->
                            @yield('feature')
                        </div><!--features_items-->

                        <div class = "recommended_items"><!--recommended_items-->
                            @yield('popular_items')
                        </div><!--/recommended_items-->

                    </div>
                </div>
            </div>
        </section>

        <footer id = "footer"><!--Footer-->
            <div class = "footer-top">
                <div class = "container-fluid">
                    <div class = "row">
                        <div class = "col-sm-3">
                            <i class = "fa fa-plane foo_icons" aria-hidden = "true"></i>
                            <div class = "companyinfo">
                                <h2>The Most Advanced Filter option.</h2>
                                <p>The most advanced filter options are addred so that you can compare and choose as per best possible requirement.</p>
                            </div>
                        </div>
                        <div class = "col-sm-3">
                            <i class = "fa fa-umbrella foo_icons" aria-hidden = "true"></i>
                            <div class = "companyinfo">
                                <h2>100% Quality Guaranteed.</h2>
                                <p>We are conscious about quality before handed over to the customer.</p>
                            </div>
                        </div>
                        <div class = "col-sm-3">

                            <i class = "fa fa-cogs foo_icons" aria-hidden = "true"></i>
                            <div class = "companyinfo">
                                <h2>Not sure about quality ?</h2>
                                <p>If you are not sure about quality, then you can order few of your selected products to choose your best one from them.</p>
                            </div>
                        </div>
                        <div class = "col-sm-3">
                            <i class = "fa fa-star foo_icons" aria-hidden = "true"></i>
                            <div class = "companyinfo">
                                <h2>New products are added every week to offer you new collection as always.</h2>
                                <p>To keep our customer up-to-date, we always update our product list .</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class = "footer-widget">
                <div class = "container">
                    <div class = "row">
                        <div class = "col-md-3">
                            <div class = "single-widget">
                                <h2>About Us</h2>
                                <p>Ronnie Area
                                    is an open market place where
                                    there is an option not only to buy but also promote products to sell online in a very convenient way .</p>
                            </div>
                        </div>
                        <div class = "col-md-3">
                            <div class = "single-widget">
                                <h2>COLUMN HEADING</h2>
                                <ul class = "nav nav-pills nav-stacked">
                                    <li><a href = "{{URL::to('/')}}">Home</a></li>
                                    <li><a href = "#">About Us</a></li>
                                    <li><a href = "#">Delivery Information</a></li>
                                    <li><a href = "#">Privacy & Policy</a></li>
                                    <li><a href = "#">Terms & Condition</a></li>
                                    <li><a href = "#">Contact Us</a></li>
                                    <li><a href = "#">Site Map</a></li>
                                </ul>
                            </div>
                        </div>
                        
                                <div class = "col-md-3">
                                <div class = "single-widget">
                                <h2>LIKE US ON FACEBOOK</h2>
                                <a href = "https://www.facebook.com/profile.php?id=100009437623496">
                                <div class = "bd">
                                <p>Ronnie Area</p>
                                </div>
                                </a>
                                </div>
                                </div>
                        
                        <div class = "col-md-3">
                            <div class = "single-widget">
                                <h2>NEWSLETTER</h2>
                                <form action = "#" class = "searchform">
                                    <input type = "text" placeholder = "Your email address" />
                                    <button type = "submit" class = "btn btn-default"><i class = "fa fa-arrow-circle-o-right"></i></button>
                                    <p>Get the most recent updates from <br />our site and be updated your self...</p>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class = "footer-bottom">
                <div class = "container">
                    <div class = "row">
                        <p class = "pull-left">Copyright ©  <?php echo date('Y');
                                    ?>  All rights reserved.</p>
                        <p class="pull-right">Developed by <span><a target="_blank" href=""><b>Ronnie</b></a></span></p>
                    </div>
                </div>
            </div>

        </footer><!--/Footer-->

        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>                    
        <script src="{{asset('public/js/jquery.js')}}"></script>
        <script src="{{asset('public/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>
        <script src="{{asset('public/css/jquery.bxslider.min.js')}}"></script>
        <script src="{{asset('public/js/jquery.scrollUp.min.js')}}"></script>
        <script src="{{asset('public/js/price-range.js')}}"></script>
        <script src="{{asset('public/js/jquery.prettyPhoto.js')}}"></script>
        <!-- The plugin source -->
        <script src="{{asset('public/js/share/Shareplugin.js')}}"></script>
        <!-- Demo page source -->
        <script src="{{asset('public/js/share/sharedemo.js')}}"></script>
        <script src="{{asset('public/js/main.js')}}"></script>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
                                    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
                                    (function () {
                                        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                                        s1.async = true;
                                        s1.src = 'https://embed.tawk.to/58dd135ef7bbaa72709c3470/default';
                                        s1.charset = 'UTF-8';
                                        s1.setAttribute('crossorigin', '*');
                                        s0.parentNode.insertBefore(s1, s0);
                                    })();
        </script>
        <!--End of Tawk.to Script-->

        <script type="text/javascript">
            $(document).ready(function () {
                $('.bxslider').bxSlider({
                    auto: true,
                    //                           autoControls: true
                });
            });
        </script>

        <script type="text/javascript">
            $("#password").password('toggle');
        </script>
    </body>
</html>