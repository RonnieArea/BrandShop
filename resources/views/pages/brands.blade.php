@extends('welcome')
@section('title_name')
Brands
@stop
@section('brands')
<h2>Brands</h2>
<div class="brands-name">
    <ul class="nav nav-pills nav-stacked">
        <li><a href="#"> <span class="pull-right">(50)</span>Acne</a></li>
        <li><a href="#"> <span class="pull-right">(56)</span>Grüne Erde</a></li>
        <li><a href="#"> <span class="pull-right">(27)</span>Albiro</a></li>
    </ul>
</div>
@stop