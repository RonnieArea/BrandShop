@extends('welcome')
@section('title_name')
Return Policy
@stop

@section('slider')
<h1 class="text-center">Returns Policy</h1>
<h3>Return Policy</h3>
<p>Your complete shopping satisfaction is our No.1 Priority. We want you happy each and every time you buy from us. However if due to some unforeseen situation you are not happy with the product then the following terms apply for a mutually consented resolution of the situation.
We do our best to make sure you never need to use this policy. But still for theory purpose here it is :-
If an item you ordered from Ronniearea.tk does not meet your expectations do write to us and we will happily try to assist you.
We are dedicated at Ronniearea.tk to offer you the best quality in our products and our services. We know that mistakes are sometimes made though, and we are here to help remedy those problems.
We take stringent measures to ensure that the items delivered to you are in perfect condition. However, there is a remote possibility that :
<br>- The item may be damaged during transit.
<br />- Or there might be a manufacturing defect.
<br>- Or that a wrong item is delivered to you by mistake.
</p>
<br>

<h4>Item received in damaged condition / manufacturing defect or wrong item -</h4>
<p>If you have received a damaged item or if the item you received was not the item you ordered or if it was misrepresented on our site, please contact us by email within 24 hours of your receipt of the product. It is important that you save all packing materials as well as the item itself. When you reach us, we'll give you instructions on how to proceed.
<br>- We will express ship a replacement item at the earliest possible with no extra shipping cost to you
<br>- If the original ordered item is no longer possible then we will request you to choose an alternate item and express ship the same to you with no extra shipping cost to you. We will communicate with you about your wishes to make sure you will be happy with the exchange.
<br>- If you do not want any alternate item then we will refund you in full with no deduction whatsoever.
<br>- Or any other amicable resolution as mutually agreed with you.</p>
<br>

<h4>Item received in damaged condition / manufacturing defect or wrong item -</h4>
<p>
    If you have received a damaged item or if the item you received was not the item you ordered or if it was misrepresented on our site, please contact us by email within 24 hours of your receipt of the product. It is important that you save all packing materials as well as the item itself. When you reach us, we'll give you instructions on how to proceed.
    <br>- We will express ship a replacement item at the earliest possible with no extra shipping cost to you
    <br>- If the original ordered item is no longer possible then we will request you to choose an alternate item and express ship the same to you with no extra shipping cost to you. We will communicate with you about your wishes to make sure you will be happy with the exchange.
    <br>- If you do not want any alternate item then we will refund you in full with no deduction whatsoever.
    <br>- Or any other amicable resolution as mutually agreed with you.
</p>
<br>

<h4>Here are some important & helpful guidelines on Returns -</h4>
<p>
    - Always email us first if you have a problem on <a href="Ronniearea.tk">Ronniearea.tk</a>
    <br>- Always be sure to report your problem within 24 hours of receiving the item in question
    <br>- Never send back a item without consulting with us. It's very important that you DO NOT send the item back to us until you have verified with us and no credit or replacement shall be given on such items.
    <br>- Always report any and all problems associated with a single order/package delivery at the same time. Be as specific as possible when describing the problem, as all returned items are examined for defects / variations.
    <br>- Always return your items using registered airmail / courier as advised in our return authorization mail.
    <br>- Any shipping costs of sending back  above what customer paid for shipping charges while receiving them  are the responsibility of the customer.
    <br>- Please return within 7 working days of confirmation from our end, and with original packing & in original condition (please, no worn, washed, used or altered items). Please ensure to include a copy of the invoice you received.
    <br>- We will examine the product  you return, figure out what went wrong, and take immediate steps to prevent such problems from reoccurring.
    <br>- As safe delivery to you is our responsibility, in the same way sending back safely to us is your responsibility. If something happens in transit and the package doesn't get delivered to us, we cannot help you. So make sure to send thru suggested postal service in our return authorization mail only and forward the details to us and keep the airway bill or postal receipt till it gets delivered to us.
    <br>- Please mark the items as defective returns and not for sale and declare a low nominal value.
</p>
<br>

<h4>For any complain / dissatisfaction with sizing and styling  - <br>
If mistake  is ours -</h4>
<p>
    - We will do free alterations & modifications. We will bear the shipping charges
    <br>- Or send free replacement if not possible to repair. We will bear shipping charges
    <br>- If replacement is no longer available then we will request you to choose some other alternative item    from our website.
    <br>- No refund can be issued under any circumstances.
    <br>- We will request the customer to ship back the item to us for alterations at our cost or arrange a    pick up from their address and send them back after correction or if not possible to correct then ship replacement item with no extra cost to the customer.
</p>
<br>

<h4>If mistake  is yours -</h4>
<p>
    - Still we will do free alterations & modifications
    <br>- We are the only company to offer free alteration facility to our valued customers. No questions asked. In some rare cases when additional original product  is no longer available then we might have to use matching product  for alteration.
    <br>- Customer will bear both ways shipping charges and in very rare cases cost of replacement of the product.
    Not withstanding the above or any other provision, we are under no obligation to accept any returns or provide any replacements or exchange or store credit. We have sole discretion in deciding whether to accept the returns or not depending on the merit of the case.
</p>
<br>

<h4>Item availability failure -</h4>
<p>
    In case of multiple goods being ordered, if the delivery is not possible for certain items, customers will be requested to select alternate items or if no alternate selection is desired by customer then s/he will be promptly refunded against the non-delivered goods or services along with their shipping charges.
</p>
<br>

<h4>Credit Issues -</h4>
<p>
    All credits to be posted to a customer's credit card account takes about 3 to 5 business days to be processed. The corresponding bank of the customers may take about 2 business days to actually post these to the customer's accounts from the date we processed the credits. Customers can check these credits in their next credit card statement.
</p>
<br>

<h4>Be extra alert when accepting parcel from courier -</h4>
<p>
    If the outer packing is damaged or tampered with, do verify the items quality and quantity with invoice before accepting the package from the courier agency. In case of any discrepancy, either refuse to accept delivery or accept delivery only after putting suitable remark on the proof of delivery. Also lodge proper complain with the local office of the courier agency, so that we may pursue them.
</p>
<br>


<h4>Color & description disclaimer -</h4>
<p>
    The details of the products or product specifications (for instance weight, color, handwork details, size, etc.) quoted with the product displays are only approximate values.
While every endeavor has been made to accurately reproduce colors, there may be minor variations in color of the actual product because of the nature of fabric dyes, weather at the time of dying and differences in display output due to lighting and digital photography and color settings and capabilities of monitors.
A customer must place an order keeping in mind this minor variation in color as seen on a computer screen against the actual color of the outfit received.
    <br><a href="Ronniearea.tk"><b>Ronniearea.tk</b></a> firmly believes that all the customers who order online are aware that colors seen on a monitor will be slightly different as compared to the actual outfits or accessories ordered.
    <br>It is practically impossible for <a href="Ronniearea.tk"><b>Ronniearea.tk</b></a> to replicate the same colors on a product  as seen on your monitor. A variation in the shade selected by you is considered as a normal practice as these products have a tendency to reflect different shades of a color under different light and weather, type of camera used for photography or type and settings on the computer monitor.
    <br>We wish to clarify and ascertain that every customer who orders any product  from <a href="Ronniearea.tk"><b>Ronniearea.tk</b></a> is aware of this genuine problem.
</p>
<br>


<h4>While ordering on internet, we suggest that customers should be extra careful of following -</h4>
<p>
    1.Red, maroon and orange colors have a higher tendency to reflect a different shade than other colors. For example even if you yourself try to photograph from a digital camera or scan a red garment, in most of the cases it will show either as maroon or orange on the computer monitor and vice versa. Though imaging technology has advanced, still no fool proof solution to this problem is available at the moment.
    <br>2.Many a times green and blue shades also overlap. same is the case with offwhite, white and cream colors.
    <br>3.Sea Green color sometimes looks Aqua blue and the other way around.
</p>
<br>


@stop
