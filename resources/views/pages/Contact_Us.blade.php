@extends('welcome')

@section('title_name')
Contact Us
@stop
@section('content')
<section id="goole-map">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <!--<iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/?ie=UTF8&amp;ll=40.959289,29.082184&amp;spn=0.012963,0.022659&amp;z=14&amp;output=embed"></iframe>-->            
            <!--<iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/?ie=UTF8&amp;ll=23.887942,90.386183&amp;spn=0.012963,0.022659&amp;z=14&amp;output=embed"></iframe>-->            
            <iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/?ie=UTF8&amp;ll=23.7503393,90.3914455&amp;spn=0.012963,0.022659&amp;z=14&amp;output=embed"></iframe>            
        </div>
    </div>
        <br><br>
    <div class="row">  	
        <div class="col-sm-8">
            <div class="contact-form">
                <h2 class="title text-center">Get In Touch</h2>
                <h4 style="color: green">
                    <?php 
                    $message = Session::get('message');
                    if($message){
                        echo $message;
                        Session::put('message',null);
                    }
                    ?>
                </h4>
                <div class="status alert alert-success" style="display: none"></div>
                {!! Form::open(['url' => '/contact-send', 'method' => 'post', 'class' => 'contact-form row', 'name' => 'contact-form']) !!}
                <div class="form-group col-md-6">
                        <input type="text" name="user_name" class="form-control" required="required" placeholder="Name">
                    </div>
                    <div class="form-group col-md-6">
                        <input type="email" name="user_email" class="form-control" required="required" placeholder="Email">
                    </div>
                    <div class="form-group col-md-12">
                        <textarea name="user_message" id="message" required="required" class="form-control" rows="8" placeholder="Your Message Here"></textarea>
                    </div>                        
                    <div class="form-group col-md-12">
                        <input type="submit" name="submit" class="btn btn-danger pull-right" value="Submit">
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-sm-4">
            <div class="contact-info">
                <h2 class="title text-center">Contact Info</h2>
                <address>
                    <p></p>
                    <p>Ekushey Television (ETV)</p>
                    <p>Jahangir Tower, 10,</p>
                    <p>Kawran Bazar, Dhaka.</p>
                    <p>Mobile: +88 017XX XXX XXX</p>
                </address>
                <div class="social-networks">
                    <h2 class="title text-center">Social Networking</h2>
                    <ul>
                        <li>
                            <a href=#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>    			
    </div>  

</div>

</section>
@stop
