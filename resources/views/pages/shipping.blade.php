@extends('welcome')
@section('title_name')
Shipping Info
@stop

@section('content')

<div class="header_top text-center">
    <h3 style="color: white">Shipping Information</h3>
    </div>

<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-offset-2">
               
                <div class="signup-form"><!--sign up form-->
                    <h2>Shipping Information</h2>
                    
                    {!! Form::open(['url' => '/save-shipping', 'method' => 'post']) !!}
                        
                    <input name="first_name" type="text" placeholder="First Name" required=""/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <input name="last_name" type="text" placeholder="Last Name"/>
                        <input name="company_name" type="text" placeholder="Company Name"/>
                        
                         <!--email address validation start-->
                        <input name="email_address" onblur="makerequest(this.value, 'res');" type="text" id="email_address" placeholder="Email Address"/>
                        <span id="res" style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <!--email address validation end-->

                        
                        <input name="address" type="text" placeholder="Enter your Address" required=""/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <input name="mobile" type="text" placeholder="Enter your Mobile" required=""/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <input name="city" type="text" placeholder="Enter your City" required=""/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <input name="zip_code" type="text" placeholder="Zip / Postal Code"/>

                        <select name="country" id="">
                            <option value="">Select Your Country</option>
                            <option value="1">Bangladesh</option>
                        </select>
                        <br /><br /><br /><br />
                        <button type="submit" id="c_buttons" class="btn btn-danger">Continue</button>
                    {{ Form::close() }}
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->


@stop

