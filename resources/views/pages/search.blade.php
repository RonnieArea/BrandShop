@extends('welcome')
@section('slider')

@foreach($v_products as $products)
<div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                @if($products->blog_image == null)
                <p style="color: #f34f4f;">no Image here</p>
                @else
                <img src="{{ asset($products->blog_image) }}"  alt="" height="250px"/>
                @endif
                <!--<img src="images/home/product1.jpg" alt="" />-->
                <h2>{{$products->price}}.Tk</h2>
                <p>{{$products->short_description}}</p>
                <a href="{{URL::to('/add-to-cart/'.$products->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
            </div>
            <div class="product-overlay">
                <div class="overlay-content">
                    <h2>{{$products->price}}.Tk</h2>
                    <p>{{$products->short_description}}</p>
                    <a href="{{URL::to('/add-to-cart/'.$products->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
            </div>
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <li>
                    <span class="socialShare"> <a href="{{URL::to('/wishlist/'.$products->product_id)}}"></a></span>
                </li>
                <li><a href="{{URL::to('/details/'.$products->product_id)}}"><i class="fa fa-info"></i>Details</a></li>
            </ul>
        </div>
    </div>
</div>
@endforeach
@stop