@extends('welcome')
@section('title_name')
Product Details
@stop
@section('slider')

<div class="product-details"><!--product-details-->
    <div class="col-sm-5">
        <div class="view-product">
            <img src="{{asset($fulldetails->blog_image)}}" alt="" />

        </div>
    </div>
    <div class="col-sm-7">
        <div class="product-information"><!--/product-information-->
            <h2>{{$fulldetails->product_name}}</h2>
            <p>Web ID: #{{$fulldetails->product_id}}</p>

            
            {{ Form::open(['url' => '/add-to-cart/'.$fulldetails->product_id, 'method' => 'post']) }}
            <span>
                <span>{{$fulldetails->price}} Tk.</span>
                <label>Quantity:</label>
                <input type="text" name="qty"  value="1" />
                <input id="proID" type="hidden" name="product_id" value="{{$fulldetails->product_id}}" />
                
                <button type="submit" class="btn btn-fefault cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
            </span>
            {{ Form::close() }}

            <p><b>Availability:</b> 
                @if($fulldetails->stock<5)
                    {{$fulldetails->stock}} Items (Hurry Up)
                
                @elseif($fulldetails->stock>0)
                     {{$fulldetails->stock}} in stock
                @else
                    Out of Stock
                @endif
            </p>

            <p>{{$fulldetails->long_description}}</p>
        </div><!--/product-information-->
    </div>
</div><!--/product-details-->


@stop
