@extends('welcome')
@section('title_name')
Cart
@stop
@section('slider')

<div class="review-payment">
    <h2>Review Your Shopping</h2>
</div>

<div class="table-responsive cart_info">
    <table class="table table-condensed">
        <thead>
            <tr class="cart_menu" style="background: #eb5e5e;color: #fff;font-weight: bolder;">
                <td class="image">Item</td>
                <td class="description">Description</td>
                <td class="price">Price</td>
                <td class="quantity">Quantity</td>
                <td class="total">Total</td>
                <td></td>
            </tr>
        </thead>
        <?php
        $contents = Cart::content();
//        echo '<pre>';
//        print_r($contents);
        ?>
        <tbody>
            @foreach($contents as $v_content)
            <tr>
                <td class="cart_product">
                    <a href="#"><img src="{{URL::to($v_content->options['blog_image'])}}" alt="product" width="100px"/></a>
                </td>
                <td class="cart_description">
                    <h4><a href="">{{$v_content->name}}</a></h4>
                    <p>Web ID: #{{$v_content->id}}</p>
                </td>
                <td class="cart_price">
                    <p> BDT. {{$v_content->price}}</p>
                </td>
                <td class="cart_quantity">
                    {{ Form::open(['url' => '/update-cart', 'method' => 'post']) }}
                    <div class="">
                        <input class="cart_quantity_input" type="text" name="qty" value="{{$v_content->qty}}">
                        <input  type="hidden" name="row_Id" value="{{$v_content->rowId}}">
                        <br><br>
                        <button type="submit" class="cart"> Update </button>
                    </div>
                    {{ Form::close() }}
                </td>
                <td class="cart_total">
                    <p class="cart_total_price">{{$v_content->price * $v_content->qty}} Tk.</p>
                </td>
                <td class="cart_delete">
                    <a class="cart_quantity_delete" href="{{URL::to('/delete-to-cart/'.$v_content->rowId)}}"><i class="fa fa-times"></i></a>
                </td>
            </tr>         

            @endforeach


            <tr>
                <td colspan="4">&nbsp;</td>
                <td colspan="2">
                    <table class="table table-condensed total-result">
                        <tr>
                            <td>Cart Sub Total</td>
                            <td>{{Cart::subtotal()}} Tk</td>
                        </tr>
                        <tr>
                            <td> Tax</td>
                            <td>00 </td>
                        </tr>
                        <tr class="shipping-cost">
                            <td>Shipping Cost</td>
                            <td>Free</td>										
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td><span>{{Cart::total()}} Tk</span></td>
                        </tr>
                    </table>
                </td>
            </tr>

        </tbody>
    </table>
    <a href="{{URL::to('/')}}"> <button type="submit" class="btn btn-danger">Continue Shopping</button></a>
    <?php
    $customer_id = Session::get('customer_id');
    $shipping_id = Session::get('shipping_id');
    if($customer_id != NULL && $shipping_id != NULL){
    ?>
    <a href="{{URL::to('/payment')}}"> <button type="submit" class="btn btn-danger" style="float: right">Proceed to Payment</button></a>
    <?php }
            elseif($customer_id != NULL && $shipping_id == NULL){
        ?>
    
    <a href="{{URL::to('/shipping-address')}}"> <button type="submit" class="btn btn-danger" style="float: right">Proceed to Shipping</button></a>
    <?php }else {?>
    <a href="{{URL::to('/checkout')}}"> <button type="submit" class="btn btn-danger" style="float: right"> Proceed to Checkout</button></a>
    <?php }?>
</div>


@stop
