@extends('welcome')
@section('title_name')
Delivery Info
@stop
@section('slider')
<h1 class="text-center">Delivery Information</h1>
<h4>Where  we deliver ?</h4>
<p>We deliver to all and any postal address around the world. Courier companies do not deliver to P.O.Box address, so we request you to provide full street address with pin code / zip code.
    Please note that residential shipments to USA & some other countries will be left at door if someone is not there to receive the package or no one answers the door bell. Our courier agents USPS / DHL / FEDEX / TNT / UPS / SpeedPost etc will not be able to provide sign confirmation of delivery made in such cases.
    To ensure that no disputes arise due to this, kindly make sure that someone is there to receive the package when courier delivery is attempted.
    No deliveries are made on Saturdays and Sundays in most countries where 5 day work culture. In many Islamic countries Fridays are normal off days. Some times we can try to organize deliveries on Saturdays also on some extra cost on case to case basis.
    Further deliveries are generally made during normal working hours of courier companies and these vary from country to country.
    Please note that there are no shipments made from the store on Sundays and public holidays as our logistics partner, does not make any pick-ups on those days.
</p>
<br><br>
<h4>Calculating cost -</h4>
<p>Shipping cost is not built in each items price. You get the benefit of reduced shipping cost on each additional item. Fair & transparent shipping rates.
Please also note that the shipping rates are weight based. The weight of any product can be found on its detail page. To match the policies of the shipping companies we use, all weights will be rounded up to the next 500 grams.
International shipping and handling rates is based on the size of your order: the larger you order, the lower the rate.
</p>

<br><br>
<h4>Shipping gifts to Bangladesh  -</h4>
<p>We take great pride in being able to offer FREE SHIPPING to any address in Bangladesh   against package order to our customers with family and friends back home. To ensure the quickest confirmed delivery on all orders ,  please be sure to include the COMPLETE postal address with the correct PIN CODE. We cannot be responsible for postal delays resulting from incomplete address listings.
Also be aware that all shipments to any Girls College/School/Hostel are placed at the customer's risk. If the school's warden does not accept your package or it is refused for any reason, Ronniearea.tk will under no circumstances issue a refund or credit to the customer's account, nor shall  Ronniearea.tk attempt re-shipment of the same order. For a special or preferred delivery date, the sender must mention the delivery date in the gift message box when placing their order. If the order does not contain any delivery date, then it will be delivered at the first opportunity, as soon as possible.</p>

<br><br>
<h4>Transit times -</h4>
<p>We ship all orders by fastest possible air courier services like USPS, DHL, TNT, SpeedPost etc and they deliver within 48-72 hours across USA, Europe and all Major cities of Middle and Far East.
    If you are looking for alternate shipping options, then cheaper and slower shipping options can be suggested. On request, we can also make special quotes for shipping if the merchandise order amount is large. please write on <a href="Ronniearea.tk">Ronniearea.tk</a>  for more details.
If you ordered two or more items, you may receive them in multiple boxes on different days, because of varying item availability and shipping locations. Often, products will be shipped separately from one another for added safety in transit, so you may receive more than one delivery to your address.
Please note that all the products will be shipped from  Bangladesh .</p>


<br><br>
<h4>Transit Risk -</h4>
<p><a href="Ronniearea.tk">Ronniearea.tk</a>   is shipping all parcels as fully insured at no extra cost to customers. If courier company fails to deliver, you do not suffer any loss. In-transit risk is all ours. But Ronniearea.tk   will not responsible for lost and stolen packages or any full or partial damages to the package after being left at customer's address by postal / courier agency.</p>


<br><br>
<h4>Shipment and tracking details :</h4>
<p>We will send you an email regarding the shipment of your order, as soon as the items are handed over to the Courier. These alert emails will contain the tracking number and courier companies website details along with expected date of delivery. you may also check the same on our order status page.
Tracking numbers for orders shipped might take 24 business hours to become active on the courier websites.</p>


<br><br>
<h4>Address change requests :</h4>
<p>Once an order is registered, you cannot make any alterations. However, address alteration requests may be accommodated within 24 hours of placing the order. You may send your alteration request to <a href="info@Ronniearea.tk">info@Ronniearea.tk</a>   and the needful will be done.</p>


<br><br>
<h4>Multiple address order -</h4>
<p>Currently this option is not available. However, if you want to send the product to different addresses, you can place Multiple Orders.</p>


<br><br>
<h4>Incorrect or incomplete address -</h4>
<p>Please note: some courier companies charge a penalty for incorrect shipping addresses, wherein the address and zip code do not match. The customer will, without exception, bear the cost of any such penalties and/or fees, not Ronniearea.tk  .
Please make sure your shipping address is correct.
In the event of a reshipment of the same order, customers will be responsible to pay re-shipping charges for the reshipment.</p>


<br><br>
<h4>Packing method -</h4>
<p>All products are first polythene packed then put in corrugated box. All orders are shipped by Ist class air courier services and home delivered within approximately 3-5 working days after dispatch of the shipment.</p>


<br><br>
<h4>Time to Ship -</h4>
<p>For each item we have mentioned time to ship along with the item image. These are indicative & approx values only and items can be shipped earlier or later also on case to case basis depending on availability of item and other factors.
It is advisable to email us with the item codes to get more accurate time to ship estimate.
We are proud to offer such shipping values to our customers, especially when paired with our fast delivery times. In most cases, your order will be processed within 48-72 hours of your placing it. You can expect to receive your order, on average, within 7 -15 business days of placing the order.</p>


<br><br>
<h4>What if any Delays -</h4>
<p>
    Time to ship mentioned on our website against each item are indicative and approximate values only and some times due to situations beyond our control, delays may occur. No refund, returns, replacement & exchange will be entertained for this reason.
However benefits such as some store credit to be used for future orders, free gift etc can be allowed on case to case basis.
If, for any reason, there is going to be a delay in shipping because a item is out of stock or If there is an unusual delay in filling an order, we will notify you by e-mail
However any dates for delivery are approximate only and we are not liable for any delay in delivering your order or any part of it. We may choose to deliver your order in separate parts . In some cases we may offer to refund you for unfulfilled part of the order.
</p>


<br><br>
<h4>VAT & Custom Duties -</h4>
<p>VAT / Custom Taxes and Import Duties are not in our control. They vary according to the rules of different countries and must be paid directly by the buyer. VAT / Custom Taxes and Import Duties are not included in our ordering process, but may be charged to you by your government.
International customers are responsible for any customs or duty fees levied by their country. We have no control over these charges and cannot predict where they will occur, as policies vary greatly from nation to nation.
They are entirely your responsibility. The courier company may also charge some additional service charges over and above custom duty on case to case basis.</p>


<br><br>
<h4>Express ship section -</h4>
<p>Express section ( Ready to Ship Section ) is a special section that allows you to place a last minute order ensuring the delivery of the product within the next 5-7 days. Only specified products are listed under the 'Express Section' the list of which is provided online. For further information, please view the Express Section.</p>

@stop
