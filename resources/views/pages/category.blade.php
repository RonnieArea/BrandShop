@extends('welcome')
@section('title_name')
Home
@stop
@section('category')

<h2>Category</h2>
<?php
$category_list = DB::table('tbl_category')
        ->where('publication_status', 1)
        ->get();
?>
<div class="panel-group category-products" id="accordian"><!--category-productsr-->

    <div class="panel panel-default">
        
            @foreach($category_list as $cat_list)
        <div class="panel-heading">
            <h4 class="panel-title"><a href="{{URL::to('/category-product/'.$cat_list->category_id)}}">{{$cat_list->category_name}}</a></h4>
        </div>
            @endforeach
        
    </div>

</div><!--/category-products-->
@stop