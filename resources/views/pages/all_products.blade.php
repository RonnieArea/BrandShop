@extends('admin.admin_master')

@section('title_name')
All products
@stop

@section('slider')
@foreach($all_product as $single_product)


<div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                @if($single_product->blog_image == null)
                <p style="color: #f34f4f;">no Image here</p>
                @else
                <img src="{{ asset($single_product->blog_image) }}"  alt="" height="250px"/>
                @endif
                <!--<img src="images/home/product1.jpg" alt="" />-->
                <h2>{{$single_product->price}}.Tk</h2>
                <p>{{$single_product->short_description}}</p>
                <a href="{{URL::to('/add-to-cart/'.$single_product->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
            </div>
            <div class="product-overlay">
                <div class="overlay-content">
                    <h2>{{$single_product->price}}.Tk</h2>
                    <p>{{$single_product->short_description}}</p>
                    <a href="{{URL::to('/add-to-cart/'.$single_product->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
            </div>
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <!--<p><span class="socialShare">  The share buttons will be inserted here  </span></p>-->
                <li>
                    <span class="socialShare"> 
                        <a href="{{URL::to('/wishlist/'.$single_product->product_id)}}" ></a>
                    </span>
                </li>
                <li><a href="{{URL::to('/details/'.$single_product->product_id)}}"><i class="fa fa-info"></i>Details</a></li>
            </ul>
        </div>
    </div>
    
    
</div>
@endforeach

@stop
