@extends('welcome')

@section('slider')

<?php
 $slider = DB::table('tbl_product')
                ->where('product_status', 1)
                ->where('is_featured', 1)
                ->orderBy('product_name', 'desc')
                ->limit(5)
                ->get();
?>
<!--<div class="col-sm-12" style="max-height: 350px">-->
    <ul class="bxslider text-center">
         @foreach($slider as $v_slider)
        <li>
            <div class="col-sm-6">
                <br><br><br>
                <h2>{{$v_slider->product_name}}</h2>
                  <p>{{$v_slider->long_description}}</p>
                  <a href="{{URL::to('/add-to-cart/'.$v_slider->product_id)}}"><button type="button" class="btn btn-danger get">Get it now</button></a>
             </div>
            <div class="col-sm-6">
                <img src="{{asset($v_slider->blog_image)}}" class="" alt="" height="300px"/>
            </div>
        </li>
        @endforeach
    </ul>

<!--</div>-->
@stop
