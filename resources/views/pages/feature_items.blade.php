@extends('welcome')

@section('feature')
<h2 class="title text-center">Features Items</h2>
<?php
//$feature_list = DB::table('tbl_product')
//        ->where('publication_status', 1)
//        ->where('is_featured', 1)
//        ->get();
?>

@if(isset($featured))
    @foreach($featured as $single_feature)
<div class="col-sm-4">
    
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                @if($single_feature->blog_image == null)
                <p style="color: #f34f4f;">no Image here</p>
                @else
                <img src="{{ asset($single_feature->blog_image) }}"  alt="" height="200px"/>
                @endif
                <!--<img src="{{asset('public/images/home/product6.jpg')}}" alt="" />-->
                <h2>{{$single_feature->price}} Tk.</h2>
                <p>{{$single_feature->short_description}}</p>
                <a href="{{URL::to('/add-to-cart/'.$single_feature->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
            </div>
            <div class="product-overlay">
                <div class="overlay-content">
                    <h2>{{$single_feature->price}} Tk.</h2>
                    <p>{{$single_feature->short_description}}</p>
                    <a href="{{URL::to('/add-to-cart/'.$single_feature->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
            </div>
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <li>
                    <span class="socialShare"> 
                        <a href="{{URL::to('/wishlist/'.$single_feature->product_id)}}"> </a>
                        </span>
                    </li>
                <li><a href="{{URL::to('/details/'.$single_feature->product_id)}}"><i class="fa fa-info"></i>Details</a></li>
            </ul>
        </div>
    </div>


</div>
@endforeach
@else
<h1>NO Items In feature</h1>
@endif
@stop
