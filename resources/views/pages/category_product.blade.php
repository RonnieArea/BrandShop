@extends('welcome')
@section('title_name')
Items
@stop

@section('slider')
<div class="col-sm-12 padding-right">

	
        
    <div class="features_items">
        @foreach($cat_info as $sin_pro)
		<div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <img src="{{asset($sin_pro->blog_image)}}" alt=""  height="250px"/>
                        <h2> {{$sin_pro->price}} Tk.</h2>
                        <p>{{$sin_pro->short_description}} </p>
                        <a href="{{URL::to('/add-to-cart/'.$sin_pro->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                    </div>
                    <div class="product-overlay">
                        <div class="overlay-content">
                            <h2>{{$sin_pro->price}} Tk.</h2>
                            <p>{{$sin_pro->short_description}}</p>
                            <a href="{{URL::to('/add-to-cart/'.$sin_pro->product_id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        </div>
                    </div>
                </div>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li>
                            <span class="socialShare"> 
                                <a href="{{URL::to('/wishlist/'.$sin_pro->product_id)}}"></a>
                            </span>
                        </li>
                        <li><a href="{{URL::to('/details/'.$sin_pro->product_id)}}"><i class="fa fa-info"></i>Details</a></li>
                    </ul>
                </div>
            </div>
        </div>
		
        @endforeach
    </div>
	

</div>


@stop
