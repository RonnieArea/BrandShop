@extends('welcome')
@section('title_name')
Checkout
@stop
@section('content')

<script type="text/javascript">
    // code for modern browsers
    var xmlhttp = false;
    try {
        // code for old IE browsers
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        //alert(xmlhttp);
        //alert("You are using Microsoft Internet Explorer");
    } catch (e) {
        //alert(e);
        //If not, then use the older active x object
        try {
            //if we are using internet Explorer
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            //alert("You are using Microsoft Internet Explorer");
        } catch (E) {
            xmlhttp = false;
        }
    }
    //alert(typeof XMLHttpRequest);
    //if we are using a non-IE browser, create a javascript instance of the object
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
        //alert("You are not using Microsoft Internet Explorer");
    }
    function makerequest(email_address, objID)
    {
        //alert(email_address);
        serverPage = 'ajax-email-check/' + email_address;
        xmlhttp.open("GET", serverPage);
        xmlhttp.onreadystatechange = function ()
        {
            // alert(xmlhttp.readyState);
            //alert(xmlhttp.status);
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                // alert(xmlhttp.responseText);
                document.getElementById(objID).innerHTML = xmlhttp.responseText;
                if (xmlhttp.responseText == 'Alreay exists') {
                    document.getElementById('c_buttons').disabled = true;
                }
                if (xmlhttp.responseText == 'Available') {
                    document.getElementById('c_buttons').disabled = false;
                }
            }
        }
        xmlhttp.send(null);
    }
</script>
<script type="text/javascript">
    function val(){
        if(frm.password.value == ""){
            alert("Enter your password");
            frm.password.focus();
            return false;
        }
        if((frm.password.value).length < 6){
            alert("Password Should be minimum 6 Charaters");
            frm.password.focus();
            return false;
        }
        if(frm.confirmpassword.value == ""){
            alert("Enter your Confirm password");
            return false;
        }
        if(frm.confirmpassword.value != frm.password.value){
            alert("Password Confirmation does not match.");
            return false;
        }
        
        if((frm.mobile.value).length < 11){
            alert("Mobile number Should be 11 digit");
            frm.password.focus();
            return false;
        }
        if((frm.mobile.value).length > 13){
            alert("Mobile number Should be 11 digit");
            frm.password.focus();
            return false;
        }
        return true;
    }
</script>

<div class="header_top text-center">
    <h3 style="color: white">Checkout</h3>
    </div>
<section id="form"><!--form-->
    
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <?php
//                    $message = Session::get('message');
//                    if($message){
//                        echo $message;
//                        Session::put('message',null);
//                    }
                    ?>
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    @if ($message = Session::get('message'))
                    <strong>Success!</strong> {{$message}}
                     @endif
                </div>
                <div class="login-form"><!--login form-->
                    <h3 style="color: red">
                        <?php
                        $exception = Session::get('exception');
                        if($exception){
                            echo $exception;
                        }
                        
                        ?>
                    </h3>
                    
                    
                    <h2>Login to your account</h2>
                    {!! Form::open(['url' => '/login-user', 'method' => 'post']) !!}
                    <input type="email" placeholder="Enter your Email Address" name="email_address"/>
                        <input type="password" placeholder="Enter your password" name="password" data-toggle="password"/>
                        <span>
                            <input type="checkbox" class="checkbox"> 
                            Keep me signed in
                        </span>
                        <button type="submit" class="btn btn-default">Login</button>
                    {{ Form::close() }}
                </div><!--/login form-->
            </div>
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->
                    <h2>New User Signup!</h2>
                    
                    {!! Form::open(['url' => '/save-customer', 'method' => 'post', 'name' => 'frm']) !!}
                        
                        <input name="first_name" type="text" placeholder="First Name" required=""/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <input name="last_name" type="text" placeholder="Last Name"/>
                        <input name="company_name" type="text" placeholder="Company Name"/>
                        
                        <!--email address validation start-->
                        <input name="email_address" onblur="makerequest(this.value, 'res');" required="" type="text" id="email_address" placeholder="Email Address"/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <span id="res" style="color: red;"></span>
                        <!--email address validation end-->

                        <input name="password" type="password" placeholder="Enter your Password" data-toggle="password"/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <input name="confirmpassword" type="password" placeholder="Confirm your Password" data-toggle="password"/>
                        
                        <input name="address" type="text"  placeholder="Enter your Address" required=""/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <input name="mobile" type="text" placeholder="Enter your Mobile" required=""/><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>
                        <input name="city" type="text" placeholder="Enter your City"/>
                        <input name="zip_code" type="text" placeholder="Zip / Postal Code" /><span style="color: red;float: right; margin-top: -40px;margin-right: -10px;">*</span>

                        <select name="country" id="" >
                            <option value="">Select Your Country</option>
                            <option value="1">Bangladesh</option>
                        </select>
                        <br /><br /><br /><br />
                        <button type="submit" id="c_buttons" class="btn btn-danger" onclick="return val();">Signup</button>
                    {{ Form::close() }}
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->


@stop
