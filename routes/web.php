<?php

/*
|--------------------------------------------------------------------------
| Web Routes for brandshowroom
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('{locale}', 'LanguageController@index');

Route::get('/', 'HomeController@index');
Route::get('/all-product', 'HomeController@all_product');
Route::get('/details/{id}', 'HomeController@details');
Route::get('/wishlist/{id}', 'CartController@wishlist');
Route::get('/category-product/{id}','HomeController@category_product');
Route::get('/search','HomeController@search');
Route::get('/deliver-info','HomeController@deliver_info');
Route::get('/return-policy','HomeController@return_policy');

/*
 * for contact form
 */
Route::get('/Contact-Us','HomeController@Contact_Us');
Route::post('/contact-send','HomeController@contact_send');
Route::get('/contact-sms','SuperAdminController@contact_sms');
Route::get('/view-sms/{id}','SuperAdminController@view_sms');
Route::get('/delete-sms/{id}','SuperAdminController@delete_sms');


/*
 * route for admin-panel
 */
Route::get('/admin-panel', 'AdminController@index');
Route::post('/admin-login-check', 'AdminController@admin_login_check');

Route::get('/dashboard', 'SuperAdminController@index');


Route::get('/add-category', 'SuperAdminController@add_category')->middleware('authenticateMiddleware');
Route::post('/save-category', 'SuperAdminController@save_category');

Route::get('/manage-category', 'SuperAdminController@manage_category')->middleware('authenticateMiddleware');
Route::get('/unpublished-category/{id}', 'SuperAdminController@unpublished_category')->middleware('authenticateMiddleware');
Route::get('/published-category/{id}', 'SuperAdminController@published_category')->middleware('authenticateMiddleware');
Route::get('/edit-category/{id}', 'SuperAdminController@edit_category')->middleware('authenticateMiddleware');
Route::post('/update-category', 'SuperAdminController@update_category');
Route::get('/delete-category/{id}', 'SuperAdminController@delete_category')->middleware('authenticateMiddleware');

Route::get('/add-product', 'SuperAdminController@add_product');
Route::post('/save-product', 'SuperAdminController@save_product');

Route::get('/manage-product', 'SuperAdminController@manage_product');

//Route::post('/manage-category-product/{id}', 'SuperAdminController@manage_category_product');

Route::get('/unpublished-product/{id}', 'SuperAdminController@unpublished_product');
Route::get('/published-product/{id}', 'SuperAdminController@published_product');
Route::get('/edit-product/{id}', 'SuperAdminController@edit_product');
Route::post('/update-product', 'SuperAdminController@update_product');
Route::get('/delete-product/{id}', 'SuperAdminController@delete_product');

/*
 * add-to-cart
 */
Route::match(['get', 'post'],'/add-to-cart/{id}', 'CartController@add_to_cart');
//Route::post('/add-to-cart/{id}', 'CartController@add_to_cart');
Route::get('/show-cart', 'CartController@show_cart');
Route::post('/update-cart', 'CartController@update_cart');
Route::get('/delete-to-cart/{id}', 'CartController@delete_cart');

/*
 * checkout
 */
Route::get('/checkout', 'CheckoutController@checkout');
Route::get('/ajax-email-check/{id}', 'CheckoutController@ajax_email_check');
Route::post('/save-customer', 'CheckoutController@save_customer');
Route::post('/login-user', 'CheckoutController@login_customer');
//Route::post('/logout-user', 'CheckoutController@logout_customer');

Route::get('/shipping-address', 'CheckoutController@shipping_address');
Route::post('/save-shipping', 'CheckoutController@save_shipping');
Route::get('/payment', 'CheckoutController@payment');
Route::post('/place-order', 'CheckoutController@place_order');
Route::get('/order-successfull', 'CheckoutController@order_successfull');

/*
 * Order Controller
 */
Route::get('/manage-order', 'OrderController@manage_order');
Route::get('/view-invoice/{id}', 'OrderController@view_invoice');
Route::get('/delete-invoice/{id}', 'OrderController@delete_invoice');
Route::get('/view-customer-details/{id}', 'OrderController@view_customer_details');


Route::get('/logout', 'SuperAdminController@logout');




